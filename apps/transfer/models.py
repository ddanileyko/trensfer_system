from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.users.models import User


class Valuta(models.Model):
    name = models.CharField(_('name'), max_length=3, db_index=True)
    rate = models.FloatField(_('rate'))
    added = models.DateTimeField(_('added'), db_index=True, auto_now_add=True)

    class Meta:
        verbose_name = _('Valuta')
        verbose_name_plural = _('Valuta')

    def __str__(self):
        return u'%s' % self.name


class Transfer(models.Model):
    from_user = models.ForeignKey(User, related_name='from_user', on_delete=models.CASCADE)
    to_user = models.ForeignKey(User, related_name='to_user', on_delete=models.CASCADE)
    from_transfer_amount = models.DecimalField(_('from transfer amount'), max_digits=9, decimal_places=2,
                                          blank=True, null=True, default=0)
    to_transfer_amount = models.DecimalField(_('to transfer amount'), max_digits=9, decimal_places=2,
                                               blank=True, null=True, default=0)
    from_balance = models.DecimalField(_('from balance'), max_digits=9, decimal_places=2, blank=True, null=True, default=0)
    to_balance = models.DecimalField(_('to balance'), max_digits=9, decimal_places=2, blank=True, null=True, default=0)
    added = models.DateTimeField(_('added'), db_index=True, auto_now_add=True)

    class Meta:
        verbose_name = _('Transfer')
        verbose_name_plural = _('Transfer')

    def __str__(self):
        return u'%s -> %s %s' % (self.from_user.email, self.to_user.email, self.from_transfer_amount)
