import requests
import json

from django.conf import settings

from django.core.management.base import BaseCommand

from apps.transfer.models import Valuta


class Command(BaseCommand):
    args = ''
    help = 'Determine the exchange rate'

    def handle(self, *args, **options):
        r = requests.get(url='https://openexchangerates.org/api/latest.json?app_id=2c3a9fbc780648ff99d18972b0feb45c')
        for val_name in settings.VALUTA:
            val_rate = json.loads(r.text)['rates'][val_name]
            val, created = Valuta.objects.get_or_create(name=val_name, defaults={'rate': val_rate})
            if not created:
                val.rate = val_rate
                val.save()