from django.test import TestCase

from ..models import Transfer
from apps.users.models import User

class UserTest(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create(email='test-1@test.com', password='1234qwer', valuta='USD', balance=1000)
        self.user_2 = User.objects.create(email='test-2@test.com', password='1234qwer', valuta='RUB', balance=10)

    def test_transfer(self):
        transfer = Transfer(from_user=self.user_1, to_user=self.user_2, from_transfer_amount=100,
                            to_transfer_amount=10, from_balance=100, to_balance=1000)
        self.assertEqual(transfer.__str__(), 'test-1@test.com -> test-2@test.com 100')
