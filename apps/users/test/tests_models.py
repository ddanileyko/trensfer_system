from django.test import TestCase

from ..models import User

class UserTest(TestCase):
    def setUp(self):
        User.objects.create(email='test-1@test.com', password='1234qwer', valuta='USD', balance=1000)
        User.objects.create(email='test-2@test.com', password='1234qwer', valuta='RUB', balance=10)

    def test_user(self):
        user_1 = User.objects.get(email='test-1@test.com')
        self.assertEqual(user_1.get_short_name(), 'test-1@test.com')
        user_2 = User.objects.get(email='test-2@test.com')
        self.assertEqual(user_2.get_short_name(), 'test-2@test.com')
