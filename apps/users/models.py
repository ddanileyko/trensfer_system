from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)
from django.db import models
from django.utils.translation import ugettext
from django.utils.translation import ugettext_lazy as _
from decimal import Decimal


class UserManager(BaseUserManager):
    def create_user(self, email, password, *args, **kwargs):
        if not email:
            msg = ugettext('Users must have an email address')
            raise ValueError(msg)

        user = self.model(
            email=UserManager.normalize_email(email)
        )
        if 'valuta' in kwargs:
            user.valuta = kwargs['valuta']
        if 'balance' in kwargs:
            user.balance = kwargs['balance']
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, password):
        user = self.create_user(email, password)
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.email_checked = True
        user.save(using=self._db)

        return user


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email'), max_length=128, unique=True, db_index=True)
    valuta = models.CharField(_('valuta'), max_length=3, db_index=True)
    balance = models.DecimalField(_('balance'), max_digits=9, decimal_places=2, blank=True, null=True, default=0)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        return '%s' % self.email

    def get_short_name(self):
        return self.email