from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.decorators import api_view, action
from decimal import Decimal, getcontext

from Api.User.serializers import UserSerializer
from apps.users.models import User
from apps.transfer.models import Valuta, Transfer


class UserAPIView(viewsets.ViewSet):
    permission_classes = (AllowAny,)

    def create(self, request, *args, **kwargs):
        user = request.data
        serializer = UserSerializer(data=user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @action(methods=['post'], detail=True, permission_classes=[AllowAny])
    def transfer(self, request, pk=None, *args, **kwargs):
        if not 'amount' in request.data:
            return Response('Request error', status=status.HTTP_400_BAD_REQUEST)

        try:
            float(request.data['amount'])
        except ValueError:
            return Response('Ivalid quantity', status=status.HTTP_400_BAD_REQUEST)

        from_user = request.user

        to_user = User.objects.filter(pk=pk).first()
        if not to_user:
            return Response('Ivalid user id', status=status.HTTP_404_NOT_FOUND)

        valuta_from = Valuta.objects.filter(name=from_user.valuta).first()
        valuta_to = Valuta.objects.filter(name=to_user.valuta).first()
        if not valuta_from or not valuta_to:
            return Response('Ivalid valuta name', status=status.HTTP_404_NOT_FOUND)

        if from_user.balance < Decimal(request.data['amount']) or Decimal(request.data['amount']) < 0:
            return Response('Insufficient funds', status=status.HTTP_403_FORBIDDEN)

        from_user.balance -= Decimal(request.data['amount'])
        from_user.save()
        to_transfer_amount = Decimal(request.data['amount']) * Decimal(valuta_from.rate) / Decimal(valuta_to.rate)
        to_user.balance += to_transfer_amount
        to_user.save()
        transfer = Transfer(from_user=from_user, to_user=to_user,
                            from_transfer_amount=Decimal(request.data['amount']), to_transfer_amount=to_transfer_amount,
                            from_balance=from_user.balance, to_balance=to_user.balance)
        transfer.save()

        return Response('Ok', status=status.HTTP_200_OK)