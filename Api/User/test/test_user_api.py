from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from apps.users.models import User

class UserCreate(APITestCase):
    def test_create_user(self):

        url = reverse('api:user-list')
        data = {"email": "test@test.com", "password": "1234qwer", "valuta": "XXX", "balance": "1000"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        url = reverse('api:user-list')
        data = {"email": "test@test.com", "password": "1234qwer", "valuta": "USD", "balance": "XXX"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        url = reverse('api:user-list')
        data = {"email": "test@test.com", "password": "1234qwer", "valuta": "USD", "balance": "-1000"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data = {"email": "test@test.com", "password": "1234qwer", "valuta": "USD", "balance": "1000"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().email, 'test@test.com')
