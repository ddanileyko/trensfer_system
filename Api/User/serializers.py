from django.conf import settings
from rest_framework import serializers

from apps.users.models import User


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True
    )
    valuta = serializers.ChoiceField(settings.VALUTA)
    balance = serializers.DecimalField(max_digits=9, decimal_places=2, min_value=0)

    class Meta:
        model = User
        fields = ['email', 'password', 'valuta', 'balance']

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)