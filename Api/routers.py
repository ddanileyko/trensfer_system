from rest_framework import routers

from Api.User.view import UserAPIView
from Api.Transfer.view import TransferViewSet


router_v1 = routers.SimpleRouter()

router_v1.register(r'user', UserAPIView, basename='user')
router_v1.register(r'transfer', TransferViewSet, basename='transfer')
