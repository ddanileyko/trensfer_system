import json
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from rest_framework.test import APIRequestFactory

from apps.users.models import User
from apps.transfer.models import Valuta, Transfer
from Api.Transfer.serializers import TransferSerializer


class TransferCreate(APITestCase):
    def setUp(self):
        url = reverse('api:user-list')
        data = {"email": "test-1@test.com", "password": "1234qwer", "valuta": "USD", "balance": "1000"}
        response = self.client.post(url, data, format='json')
        self.user_from = User.objects.get(email='test-1@test.com')

        data = {"email": "test-2@test.com", "password": "1234qwer", "valuta": "RUB", "balance": "1000"}
        response = self.client.post(url, data, format='json')
        self.user_to = User.objects.get(email='test-2@test.com')
        self.id_test_2 = User.objects.get(email='test-2@test.com').id

        valuta = Valuta(name='USD', rate=1)
        valuta.save()
        valuta = Valuta(name='RUB', rate=0.1)
        valuta.save()

    def test_create_transfer(self):

        client = APIClient()

        url = reverse('api:user-transfer', args=[self.id_test_2])
        client.force_authenticate(user=self.user_from)

        resp = client.post(url, data={'format': 'json'})
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(resp.data, 'Request error')

        resp = client.post(url, data={'format': 'json',  'amount': 'abc'})
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(resp.data, 'Ivalid quantity')

        resp = client.post(reverse('api:user-transfer', args=[123]), data={'format': 'json',  'amount': '100'})
        self.assertEqual(resp.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(resp.data, 'Ivalid user id')

        resp = client.post(url, data={'format': 'json',  'amount': '1001'})
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(resp.data, 'Insufficient funds')

        resp = client.post(url, data={'format': 'json', 'amount': '-100'})
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(resp.data, 'Insufficient funds')

        resp = client.post(url, data={'format': 'json', 'amount': '100'})
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(resp.data, 'Ok')
        user_to = User.objects.get(email='test-2@test.com')
        self.assertEqual(self.user_from.balance, 900)
        self.assertEqual(user_to.balance, 2000)
        transfer = Transfer.objects.all()
        self.assertEqual(transfer.count(), 1)
        self.assertEqual(transfer[0].from_transfer_amount, 100)

    def test_transfer_list(self):

        client = APIClient()
        client.force_authenticate(user=self.user_from)
        resp = client.post(reverse('api:user-transfer', args=[self.user_to.id]),
                           data={'format': 'json', 'amount': '100'})
        factory = APIRequestFactory()
        transfers = Transfer.objects.all()
        request = factory.get('transfer-list')
        request.user = self.user_from
        resp = client.get(reverse('api:transfer-list'))
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        serializer = TransferSerializer(transfers, context={'request': request}, many=True)
        self.assertEqual(resp.data, serializer.data)
        self.assertEqual(json.loads(json.dumps(resp.data, ensure_ascii=False, default=str))[0]['user_name'],
                         'test-2@test.com')
        self.assertEqual(json.loads(json.dumps(resp.data, ensure_ascii=False, default=str))[0]['transfer_amount'],
                         '-100.00')
        self.assertEqual(json.loads(json.dumps(resp.data, ensure_ascii=False, default=str))[0]['balance'], '900.00')
        client.force_authenticate(user=self.user_to)
        resp = client.get(reverse('api:transfer-list'))
        self.assertEqual(json.loads(json.dumps(resp.data, ensure_ascii=False, default=str))[0]['user_name'],
                         'test-1@test.com')
        self.assertEqual(json.loads(json.dumps(resp.data, ensure_ascii=False, default=str))[0]['transfer_amount'],
                         '1000.00')
        self.assertEqual(json.loads(json.dumps(resp.data, ensure_ascii=False, default=str))[0]['balance'], '2000.00')
