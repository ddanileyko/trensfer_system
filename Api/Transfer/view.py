from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import viewsets
from django.db.models import Q

from Api.Transfer.serializers import TransferSerializer
from apps.transfer.models import Transfer


class TransferViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Transfer.objects.all()
    serializer_class = TransferSerializer

    def get_queryset(self):
        qs = super(TransferViewSet, self).get_queryset()
        qs = qs.filter(Q(from_user=self.request.user.id) | Q(to_user=self.request.user.id)).order_by('-added')
        return qs