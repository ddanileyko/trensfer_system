from rest_framework import serializers

from apps.transfer.models import Transfer


class TransferSerializer(serializers.ModelSerializer):
    transfer_amount = serializers.SerializerMethodField()
    user_name = serializers.SerializerMethodField()
    user_name = serializers.SerializerMethodField()
    balance = serializers.SerializerMethodField()

    class Meta:
        model = Transfer
        fields = ['user_name', 'transfer_amount', 'balance', 'added']

    def get_transfer_amount(self, obj):
        return -obj.from_transfer_amount if self.context['request'].user.id == obj.from_user.id else obj.to_transfer_amount

    def get_user_name(self, obj):
        return obj.to_user.email if self.context['request'].user.id == obj.from_user.id else obj.from_user.email

    def get_balance(self, obj):
        return obj.from_balance if self.context['request'].user.id == obj.from_user.id else obj.to_balance