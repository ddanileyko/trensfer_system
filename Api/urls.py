from django.urls import path, include
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

from .routers import router_v1

urlpatterns = [
    path(r'v1/', include(router_v1.urls)),
    path('api-token-auth/', obtain_jwt_token, name='obtain_jwt_token'),
    path('api-token-refresh/', refresh_jwt_token),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
